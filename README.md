<div align="center">
  <img src="./static/logo.png" width="300" alt="ElectroAAC">

  A free and open-source Automatic Account Creator (AAC) written in Javascript Stack.
  <br>

  ![Badge](https://img.shields.io/github/issues/ElectroAAC/electro?color=green)
  ![Badge](https://img.shields.io/github/forks/ElectroAAC/electro)
  ![Badge](https://img.shields.io/github/stars/ElectroAAC/electro)
  ![Badge](https://img.shields.io/apm/l/vim-mode)
</div>

<br>

<h2> 👨🏾‍💻 Techs </h2>
<li> Nuxt.js </li>
<li> Vuetify.js </li>
<li> Typescript </li>
<br>

<h2> 🍸 Build Setup </h2>

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

<h2> 👤 Author </h2>

<h3> <b> Waliston Belles </b></h3>

<li> Github: <a href="https://github.com/WalistonBelles">@walistonbelles</a> </li>
<li> Linkedin: <a href="https://www.linkedin.com/in/waliston-belles-88927a212/"> Waliston Belles</a> </li>
<li> Discord:  Waliston#0145</a></li>
<br>